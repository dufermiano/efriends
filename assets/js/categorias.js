$( document ).ready(function(){
	categoriaProg();
	categoriaDevops();
	categoriaWeb();
});

function categoriaProg(){
	var base = "sobre";
	$.ajax({
		url: "index.php/Ebook/prog",
		type: "GET",
		data: "" ,
		dataType: 'json',
		success: function (data){
			$.each(data, function (i, item) {
				$('<div class="livro">').append(   
						$('<a class="desktop-only btn" href="'+base+'?cod='+item.idEbook+'">Sobre o livro</a>'),
						$('<a href="'+base+'?cod='+item.idEbook+'"><img src="'+item.Capa+'"></a>'),
						$('<h2>'+item.Titulo_Ebook+'</h2>')
				).appendTo('#programacao');
			});
		}

	});
	
}

function categoriaDevops(){
	var base = "sobre";
	$.ajax({
		url: "index.php/Ebook/devops",
		type: "GET",
		data: "" ,
		dataType: 'json',
		success: function (data){
			$.each(data, function (i, item) {
				$('<div class="livro">').append(   
						$('<a class="desktop-only btn" href="'+base+'?cod='+item.idEbook+'">Sobre o livro</a>'),
						$('<a href="'+base+'?cod='+item.idEbook+'"><img src="'+item.Capa+'"></a>'),
						$('<h2>'+item.Titulo_Ebook+'</h2>')
				).appendTo('#devops');
			});
		}

	});
}

function categoriaWeb(){
	var base = "sobre";
	$.ajax({
		url: "index.php/Ebook/web",
		type: "GET",
		data: "" ,
		dataType: 'json',
		success: function (data){
			$.each(data, function (i, item) {
				$('<div class="livro">').append(   
						$('<a class="desktop-only btn" href="'+base+'?cod='+item.idEbook+'">Sobre o livro</a>'),
						$('<a href="'+base+'?cod='+item.idEbook+'"><img src="'+item.Capa+'"></a>'),
						$('<h2>'+item.Titulo_Ebook+'</h2>')
				).appendTo('#web');
			});
		}

	});
}

