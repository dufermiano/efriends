<?php
$this->load->view ( 'plataforma/head' );
$this->load->view ( 'plataforma/nav' );
?>
    <section>
      <article>
        <h1>Programação</h1>
        <hr>
        <div class="container" id="programacao">
        </div>
        <h1>Ferramentas DevOps</h1>
        <hr>
        <div class="container" id="devops">
        </div>
        <h1>Desenvolvimento Web</h1>
        <hr>
        <div class="container" id="web">
        </div>
      </article>
    </section>
		<?php $this->load->view ( 'plataforma/footer' );?>
        <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/mobile.js')?>"></script>
        <script src="<?php echo base_url('assets/js/categorias.js')?>"></script>
  </body>
</html>
