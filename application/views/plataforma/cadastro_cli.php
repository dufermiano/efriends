<?php $this->load->view("plataforma/head");?>
<?php $this->load->view ( 'plataforma/nav' );?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/cadastro.css')?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/painel.css')?>">
<section>
      <article>
        <div class="alert" id="alert">
          <p>Para comprar ou vender e-books na E-Friends é necessario se cadastrar!</p>
        </div>
        
        <?php if($msg = get_msg()): echo '<div class="msg-box">'.$msg.'</div>'; endif;?>
        <form action="index.php/Cliente/insere_cliente" method="post">
            <h3>Informações do Cliente:</h3>
            <input type="text" placeholder="Nome" name="nome" required>
            <input type="text" placeholder="Telefone" name="telefone" required>
            <input type="email" placeholder="E-Mail" name="email" required>
            <input placeholder="CPF" type="text" name="cpf" id="cpf" required onblur="validarCPF(this.value)" onkeypress="mascara(this,mcpf)" maxlength="14">
           <div id="erro" class="msg-box" style="width: 150px; margin-left:51%; visibility:hidden;" >CPF Inválido</div>
            <h3>Usuario</h3>
            <input type="text" placeholder="Usuario" name="login" required>
            <input type="password" placeholder="Senha" name="senha" required>
            <input type="password" placeholder="Confirmar Senha" name="senha2" required>
            <select name="pergunta" required>
              <option>Pergunta de segurança</option>
              <option value="1" >Mês de aniversario?</option>
              <option value="2">Time do coração?</option>
              <option value="3">Qual o nome do seu cachorro?</option>
            </select>
            <input type="text" placeholder="Resposta" name="resposta" required>
            <input class="newsletter" type="checkbox" name="newsletter"><label>Newsletter</label><br>
            <button id="btncad">Cadastrar</button>
        </form>
      </article>
    </section>
    <footer>
      <p>&copy Copyright 2016 E-Friends</p>
    </footer>
    <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/mobile.js')?>"></script>
    <script src="<?php echo base_url('assets/js/cpf.js')?>"></script>
  </body>
</html>

