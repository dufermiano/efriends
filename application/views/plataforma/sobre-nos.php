<?php
$this->load->view ( 'plataforma/head' );
$this->load->view ( 'plataforma/nav' );
$logado = $this->session->userdata('logado');
?>
<link rel="stylesheet"
	href="<?php echo base_url('assets/css/sobre.css')?>">
<section>
	<article>


		<div class="prevacio">
			<h1>Quem somos?</h1>
			<hr>
			<br>
			<p>
				Dois estudantes do curso de Análise e desenvolvimento de sistemas na
				Fatec Zona Sul de São Paulo, que durante o desenvolvimento de seu
				trabalho de conclusão de curso, tiveram a ideia de criar uma
				plataforma para autores sem editoras venderem seus livros na Web.
				Eles queriam contribuir, de alguma forma, com a disseminação de
				conteúdos online. <br>
			</p>
			<p>
				Assim, uniram uma necessidade de mercado com um projeto opensource,
				sem fins lucrativos, que visa somente a divulgação e venda de obras
				literárias em formato de E-books.<br>
			</p>
			<p>O projeto E-friends consiste em disponibilizar uma plataforma
				Web/Mobile gratuita para que autores sem editoras possam fazer o
				upload de suas obras. Seja para comércio, pelo valor que julgarem
				justo, seja para divulgação do conteúdo, sem cobrar qualquer valor
				sobre isto.</p>
			</p>
			<br>
		</div>
		<div class="prevacio">
			<h1>E como funciona?</h1>
			<hr>
			<br>
			<p>
				É simples. Basta se cadastrar no E-friends, clicando em <a
					class="link-texto" href="http://efriends.net.br/cadastro_cli">Cadastro</a>,
				e publicar sua obra através da <?php if($logado != null):?> <a class="link-texto"
					href="<?php echo base_url('cadastro_obra')?>">Dashboard de Autor</a>.<?php else: ?> Dashboard de Autor (Necessário estar logado) 
					<?php endif;?> 
				Para comercializar, crie uma conta no <a class="link-texto"
					href="https://pagseguro.uol.com.br/registration/registration.jhtml">PagSeguro</a>,
				gere um <a class="link-texto"
					href="https://pagseguro.uol.com.br/preferencias/integracoes.jhtml">Token</a>,
				insira em sua <?php if($logado != null):?><a class="link-texto"
					href="<?php echo base_url('perfil_cli')?>">Dashboard </a> <?php else: ?> Dashboard (Necessário estar logado) <?php endif;?>  e pronto,
				todos seus livros vendidos serão contabilizados em SUA conta no
				PagSeguro! Desta forma, você faz a venda pelo tempo que quiser, e
				pelo valor que julgar justo! O E-friends é a plataforma para VOCÊ
				vender seu livro digital!
			</p>
			<br>
		</div>
	</article>
</section>
<
<div class="desktop comentarios">
	<h1>Dúvidas ?</h1>
	<hr style="border: 1px solid #c0392b;">
	<br>
	<div class="desktop fb-comments"
		data-href="http://efriends.host/sobre.html" data-numposts="5"></div>
	<!-- https://developers.facebook.com/docs/plugins/comments/ -->
</div>
<?php $this->load->view ( 'plataforma/footer' );?>
<script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/mobile.js')?>"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>
